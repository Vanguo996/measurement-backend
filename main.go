package main

import (
	"log"

	"google.golang.org/grpc"

	constant "measurement-backend/constant"
	grpc_controller "measurement-backend/controller/grpc_controller"

	ws_controller "measurement-backend/controller/ws_controller"
	pb "measurement-backend/pb"
)

func main() {
	addr := constant.HostAddr
	// addr := "172.17.0.3:52002"
	// conn, err := grpc.Dial(addr, grpc.WithInsecure(), grpc.WithBlock())
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}

	defer conn.Close()

	client := pb.NewInstrumentsControllerClient(conn)

	// grpc_controller.CloseOutputInstruments(client, constant.Instr2400)

	// grpc_controller.SweepModeStart(client, -2, 0.1, 2, 100e-3, 1)

	go grpc_controller.SimulationModeStart(client)

	ws_controller.ChatAppStart()
}
