package ws_controller

import (
	"log"
	"net/http"

	router "measurement-backend/controller/ws_controller/router"

	"github.com/gorilla/mux"
)

func init() {
	log.SetPrefix("TRACE: ")
	log.SetFlags(log.Ldate | log.Llongfile)
}

func ChatAppStart() {

	log.Println("Server will start at 8080")

	route := mux.NewRouter()

	router.AppRoutes(route)

	log.Fatal(http.ListenAndServe(":8080", route))
}
